var pc = null;
var channel = null;

function negotiate() {
    channel = pc.createDataChannel("chat");
    pc.addTransceiver('video', {direction: 'recvonly'});
    pc.addTransceiver('audio', {direction: 'recvonly'});
    return pc.createOffer().then(function (offer) {
        return pc.setLocalDescription(offer);
    }).then(function () {
        // wait for ICE gathering to complete
        return new Promise(function (resolve) {
            if (pc.iceGatheringState === 'complete') {
                resolve();
            } else {
                function checkState() {
                    if (pc.iceGatheringState === 'complete') {
                        pc.removeEventListener('icegatheringstatechange', checkState);
                        resolve();
                    }
                }

                pc.addEventListener('icegatheringstatechange', checkState);
            }
        });
    }).then(function () {
        var offer = pc.localDescription;
        return fetch('/offer', {
            body: JSON.stringify({
                sdp: offer.sdp,
                type: offer.type,
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
    }).then(function (response) {
        return response.json();
    }).then(function (answer) {
        return pc.setRemoteDescription(answer);
    }).catch(function (e) {
        alert(e);
    });
}
document.querySelector("video#video").addEventListener('mousemove', function (ev) {
    if (channel)  channel.send(JSON.stringify({action: "move", x: ev.layerX, y: ev.layerY}))
})

document.querySelector("video#video").addEventListener('mousedown', function (ev) {
    console.log(ev);
    if (channel) channel.send(JSON.stringify({action: "press","button":ev.buttons, x: ev.layerX, y: ev.layerY}))
})
document.querySelector("video#video").addEventListener("contextmenu", e => e.preventDefault());

document.querySelector("body").addEventListener("keypress" , function (ev) {
    console.log(ev);
    if (channel)  channel.send(JSON.stringify({action: "key","type":"press","key":ev.key}))
})

document.querySelector("body").addEventListener("keyup" , function (ev) {
        if (channel)  channel.send(JSON.stringify({action: "key","type":"release","key":ev.key}))

})

document.querySelector("video#video").addEventListener('mouseup', function (ev) {
    if (channel)  channel.send(JSON.stringify({action: "release","button":ev.buttons, x: ev.layerX, y: ev.layerY}))
})

function cliclsend(ev) {
    console.log(ev);
}

function start() {
    var config = {
        sdpSemantics: 'unified-plan'
    };

        config.iceServers = [{urls: ['stun:stun.l.google.com:19302']}];


    pc = new RTCPeerConnection(config);

    // connect audio / video
    pc.addEventListener('datachannel', function (event) {
        console.log('data channel')
        channel.send('tester 01');
    });
    pc.addEventListener('message', function (event) {
        console.log('data channel message')
    });
    pc.addEventListener('track', function (evt) {
        if (evt.track.kind == 'video') {
            document.getElementById('video').srcObject = evt.streams[0];
        } else {
            document.getElementById('audio').srcObject = evt.streams[0];
        }
    });

    // document.getElementById('start').style.display = 'none';
    negotiate();
    // document.getElementById('stop').style.display = 'inline-block';
}

function stop() {
    document.getElementById('stop').style.display = 'none';

    // close peer connection
    setTimeout(function () {
        pc.close();
    }, 500);
}
start()